import { UAParser } from 'ua-parser-js'
import '../styles/index.scss'

window.addEventListener('load', async () => {
  const UA = new UAParser(navigator.userAgent)

  const uaBrowser = UA.getBrowser()
  const uaOS = UA.getOS()

  const isWindows = uaOS.name === 'Windows'
  const isIOS = uaOS.name === 'iOS'
  const isAndroid = uaOS.name === 'Android'
  const isMobile = isAndroid || isIOS

  const isChrome = ['Chrome'].includes(uaBrowser.name)
  const isFirefox = ['Firefox'].includes(uaBrowser.name)

  const displayClass = (className) => {
    try {
      document.querySelectorAll(`.ua-${className}`).forEach((el) => el.classList.remove('d-none'))
    } catch (error) {
      console.warn(error)
    }
  }

  if (isMobile) {
    if (isIOS) displayClass('ios')
    if (isAndroid) displayClass('android')
    // if (isAndroid && isFirefox) displayClass('firefox') // Firefox for android supports addons
  } else {
    if (isWindows) displayClass('windows') // if windows is detected
    if (isChrome) displayClass('chrome')
    if (isFirefox) displayClass('firefox')
  }
})
