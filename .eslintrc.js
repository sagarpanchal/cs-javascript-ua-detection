module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    jest: true,
    node: true,
  },
  extends: 'eslint:recommended',
  parserOptions: {
    ecmaVersion: 2019,
    sourceType: 'module',
  },
  rules: {
    'no-console': 0,
    'no-unused-vars': 1,
    'no-useless-escape': 0,
    'require-atomic-updates': 0,
  },
}
