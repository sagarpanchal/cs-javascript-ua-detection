const path = require('path')
var webpack = require('webpack')
const TerserPlugin = require('terser-webpack-plugin')

process.env.NODE_ENV = process.env.NODE_ENV || 'development'
process.env.CVA_PORT = process.env.CVA_PORT || 3000

const config = function (mode) {
  let conf = {
    mode: mode,
    entry: () => ({
      index: './src/scripts/index.js',
    }),
    module: {
      rules: [
        {
          test: /\.m?js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [['@babel/preset-env', { targets: { node: '10' } }]],
            },
          },
        },
        {
          test: /\.html$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'html-loader',
            options: {},
          },
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.scss$/,
          use: ['style-loader', 'css-loader', 'sass-loader'],
        },
      ],
    },
    output: {
      path: path.resolve(__dirname, 'public/scripts/'),
      filename: '[name].js',
      publicPath: '/',
    },
    plugins: [],
    optimization: {
      minimizer: [new TerserPlugin()],
    },
    devServer: {
      watchOptions: { ignored: /node_modules/ },
      contentBase: 'public',
      watchContentBase: true,
      compress: true,
      hot: true,
      inline: true,
      port: process.env.CVA_PORT,
    },
  }

  if (mode === 'development') {
    conf.plugins.push(new webpack.HotModuleReplacementPlugin())
    conf.plugins.push(new webpack.NoEmitOnErrorsPlugin())
  }

  return conf
}

module.exports = config(process.env.NODE_ENV)
